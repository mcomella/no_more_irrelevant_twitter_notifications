# No More Irrelevant Twitter Notifications!
[Available on addons.mozilla.org][amo]

On social media, your feed is for discovering new content while your
notifications panel is for finding responses to actions you've taken.
However, if you don't use Twitter often,
[your notification feed is cluttered with new content that doesn't involve you][annoying]:
- "In case you missed X's Tweet"
- "Recent Tweet from X"
- "X liked a photo from Y"
- "X Tweeted after a while"
- "X follows you - join Y, Z and 123 others in following them"

This browser add-on colors these notifications differently so they're easy
to skim over. If you want to read the content, mouse over them to reveal
them.

![Screenshot of the add-on](docs/screenshot.png)

This add-on also colors promoted tweets.

Unfortunately, your unread notification count will still increase but it's
a start.

### Alternative solutions
- Click the down arrow on the notification and press "See less often":
this doesn't seem to work very well.
- Click the "Mentions" tab in your notifications instead of "All": you
miss out on likes and retweets of your content, however.
- [Fix Twitter] removes these tweets from the stream entirely, in
addition to other features. It seemed unnecessary that it frequently
polled the disk, however, so I made my own add-on.
- A [Greasemonkey script][grease] also removes tweets from the stream:
it's less accessible than an add-on, however.

In my dev tools proof of concept, removing notifications from the stream
had side effects (e.g. additional notifications don't load) so I decided
to obscure the notifications instead.

## Development
In Firefox, you can load a temporary add-on for testing.

## Publishing
`dist/bundle.xpi` will be created after:
```sh
./publish.sh
```

## License
The code is licensed under the X11 license, which is similar to the MIT license.

The implementation for this solution was inspired by https://github.com/jonsuh/fix-twitter

[amo]: https://addons.mozilla.org/en-US/firefox/addon/irrelevant-twitter-notify/
[Fix Twitter]: https://github.com/jonsuh/fix-twitter
[grease]: https://dev.to/c33s/twitter-stop-to-annoy-me-with-in-case-you-missed-it-notifications-53bp
[annoying]: https://techcrunch.com/2017/09/04/twitters-new-random-notifications-are-awful-and-i-hate-them/

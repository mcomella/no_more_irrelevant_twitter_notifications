#!/bin/bash

rm -rf dist/
mkdir dist/

zip -r -FS dist/bundle.xpi \
    src/ \
    LICENSE \
    README.md \
    manifest.json
